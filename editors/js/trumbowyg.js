(function ($) {
    "use strict";
    /* global Drupal */

    /**
     * Attach this editor to a target element.
     */
    Drupal.wysiwyg.editor.attach.trumbowyg = function (context, params, settings) {
        params.field && ($('#' + params.field, context).trumbowyg(settings));
    };

    /**
     * Detach a single or all editors.
     *
     * See Drupal.wysiwyg.editor.detach.none() for a full description of this hook.
     */
    Drupal.wysiwyg.editor.detach.trumbowyg = function (context, params, trigger) {
        if (trigger === 'serialize') {
            return;
        }

        params.field && ($('#' + params.field, context).trumbowyg('destroy'));
    };

    /**
     * Instance methods
     */
    Drupal.wysiwyg.editor.instance.trumbowyg = {
        insert: function (content) {
            var editor = document.getElementById(this.field);

            // IE support.
            if (document.selection) {
                editor.focus();
                var sel = document.selection.createRange();
                sel.text = content;
            }
            // Mozilla/Firefox/Netscape 7+ support.
            else if (editor.selectionStart || editor.selectionStart === '0') {
                var startPos = editor.selectionStart;
                var endPos = editor.selectionEnd;
                editor.value = editor.value.substring(0, startPos) + content + editor.value.substring(endPos, editor.value.length);
            }
            // Fallback, just add to the end of the content.
            else {
                editor.value += content;
            }
        },

        setContent: function (content) {
            $('#' + this.field).trumbowyg('html', content);
        },

        getContent: function () {
            return $('#' + this.field).trumbowyg('html');
        }
    };

})(jQuery);
