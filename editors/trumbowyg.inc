<?php

/**
 * @file
 * Trumbowyg.
 */

/**
 * Define a Wysiwyg editor library.
 */
function wysiwyg_trumbowyg_trumbowyg_editor() {
  $editor['trumbowyg'] = array(
    // The official, human-readable label of the editor library.
    'title' => 'Trumbowyg',
    // The URL to the library's homepage.
    'vendor url' => 'https://alex-d.github.io/Trumbowyg/',
    // The URL to the library's download page.
    'download url' => 'https://alex-d.github.io/Trumbowyg/',
    // A definition of available variants for the editor library.
    // The first defined is used by default.
    'library path' => wysiwyg_get_path('trumbowyg'),
    'libraries' => array(
      'src' => array(
        'title' => 'Source',
        'files' => array(
          'dist/trumbowyg.js',
        ),
      ),
      '' => array(
        'title' => 'Default',
        'files' => array(
          'dist/trumbowyg.min.js',
        ),
      ),
    ),
    'version callback' => 'wysiwyg_trumbowyg_editor_version',
    'load callback' => 'wysiwyg_trumbowyg_editor_load',
    'settings callback' => 'wysiwyg_trumbowyg_editor_settings',

    'versions' => array(
      '2.0.0-beta.7' => array(
        'js files' => array('trumbowyg.js'),
        'css files' => array('trumbowyg_fixes.css'),
      ),
      'v2.5.0' => array(
        'js files' => array('trumbowyg.js'),
        'css files' => array('trumbowyg_fixes.css'),
      ),
    ),
  );
  return $editor;
}

/**
 * Get the editor version.
 */
function wysiwyg_trumbowyg_editor_version($editor) {
  $bower_file = $editor['library path'] . '/bower.json';

  if (file_exists($bower_file)) {
    $bower = json_decode(file_get_contents($bower_file));

    if (isset($bower->version)) {
      return $bower->version;
    }
  }

}

/**
 * Perform additional actions upon loading this editor.
 *
 * @param string $editor
 *   A processed hook_editor() array of editor properties.
 * @param string $library
 *   The internal library name (array key) to use.
 */
function wysiwyg_trumbowyg_editor_load($editor, $library) {
  drupal_add_css($editor['library path'] . '/dist/ui/trumbowyg.css');
}

/**
 * Get the editor settings.
 */
function wysiwyg_trumbowyg_editor_settings($editor, $config, $theme) {
  $library_path = base_path() . $editor['library path'] . '/';

  return array(
    'svgPath' => $library_path . 'dist/ui/icons.svg',
  );
}

/**
 * Editor plugins.
 */
function wysiwyg_trumbowyg_editor_plugins() {
  return array();
}
